# This file is a template, and might need editing before it works on your project.
FROM python:3.8.0-slim
WORKDIR /app
ADD . /app
RUN pip install --trusted-host pypi.python.org Flask  
ENV NAME Mark
# For some other command
CMD ["python", "app.py"]
